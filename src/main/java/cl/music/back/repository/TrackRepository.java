package cl.music.back.repository;

import cl.music.back.domain.Track;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Track entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrackRepository extends MongoRepository<Track, String> {

}
