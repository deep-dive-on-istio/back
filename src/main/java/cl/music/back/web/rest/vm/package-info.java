/**
 * View Models used by Spring MVC REST controllers.
 */
package cl.music.back.web.rest.vm;
