package cl.music.back.service.impl;

import cl.music.back.domain.Track;
import cl.music.back.repository.TrackRepository;
import cl.music.back.service.TrackService;
import cl.music.back.service.dto.TrackDTO;
import cl.music.back.service.mapper.TrackMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Service Implementation for managing {@link Track}.
 */
@Service
public class TrackServiceImpl implements TrackService {

    private final Logger log = LoggerFactory.getLogger(TrackServiceImpl.class);

    private final TrackRepository trackRepository;

    private final TrackMapper trackMapper;

    public TrackServiceImpl(TrackRepository trackRepository, TrackMapper trackMapper) {
        this.trackRepository = trackRepository;
        this.trackMapper = trackMapper;
    }

    /**
     * Save a track.
     *
     * @param trackDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TrackDTO save(TrackDTO trackDTO) {
        log.debug("Request to save Track : {}", trackDTO);
        Track track = trackMapper.toEntity(trackDTO);
        track = trackRepository.save(track);
        return trackMapper.toDto(track);
    }

    /**
     * Get all the tracks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    public Page<TrackDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tracks");

        RestTemplate restTemplate = new RestTemplate();

        int start = (int) pageable.getOffset();

        String urlClient = "http://client.music:8080/client/api/tracks?limit=" + pageable.getPageSize() +"&muse=ele&offset=" + pageable.getOffset();
        //?limit=10&muse=ale&offset=10

        ResponseEntity<List<TrackDTO>> response = restTemplate.exchange(
            urlClient,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<TrackDTO>>(){});

        int end = (start + pageable.getPageSize()) > response.getBody().size() ? response.getBody().size() : (start + pageable.getPageSize());

        return new PageImpl<TrackDTO>(response.getBody().subList(start, end), pageable, response.getBody().size());

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;

    }

    /**
     * Get one track by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<TrackDTO> findOne(String id) {
        log.debug("Request to get Track : {}", id);
        return trackRepository.findById(id)
            .map(trackMapper::toDto);
    }

    /**
     * Delete the track by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Track : {}", id);
        trackRepository.deleteById(id);
    }
}
