package cl.music.back.service.mapper;

import cl.music.back.domain.*;
import cl.music.back.service.dto.TrackDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Track} and its DTO {@link TrackDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TrackMapper extends EntityMapper<TrackDTO, Track> {

    default Track fromId(String id) {
        if (id == null) {
            return null;
        }
        Track track = new Track();
        track.setId(id);
        return track;
    }
}
